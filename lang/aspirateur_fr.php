<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
		
	// B
	'bouton_aspirer_previsu' => 'Prévisualiser l\'aspiration',
	'bouton_lancer_test_aspirateur' => 'Lancer le test d\'aspirateur',
	'bouton_lancer_aspirateur_rss'=>'Créer le RSS',
	
	// E
	'explication_aspirateur_fin' => 'Prévisualiser les liens des pages à récupérer, vous pouvez aussi modifier cette liste une fois générée.',
	'explication_tmp_liste' => 'Les liens aspirés pour @url_site@ sont listés dans @url_tmp_liste@ il faut supprimer ce fichier pour recommencer l\'aspiration.',
	'explication_nombre_de_pages_rss'=>'Chaque page aspirée devient un item du flux RSS reprenant son contenu traité et ses documents en enclosures.',

	// I
	'info_result_aspiration_contenu'=>'Résultat de l\'aspiration du contenu',
	'info_result_aspiration_liens'=>'Résultat de l\'aspiration des liens de pages',
	'info_result_aspiration_documents'=>'Résultat de l\'aspiration des liens de documents',
	'info_result_aspiration_rss'=>'Résultat de l\'aspiration en RSS',
	'info_result_aspiration_pages' => 'Aspiration des pages, voir @url_rss@',
	'info_aspirateurs_pages' => 'L\'aspirateur va récupérer ',
	'info_1_page' => '1 page',
	'info_nb_pages' => '@nb@ pages',
	'info_aucune_page' => 'Aucune page',
	'info_1_contenu' => '1 contenu',
	'info_nb_contenus' => '@nb@ contenus',
	'info_aucune_contenu' => 'Aucun contenu',
	'info_1_lien' => '1 lien',
	'info_nb_liens' => '@nb@ liens',
	'info_aucun_lien' => 'Aucun lien',
	'info_1_document' => '1 document',
	'info_nb_documents' => '@nb@ documents',
	'info_aucune_document' => 'Aucun document',
	'info_result_titre'=>'Titre ',
	'info_result_contenu'=>'Contenu ',
	
	// L
	'label_nombre_de_pages_rss' => 'Nombre de pages',


	// T
	'titre_configurer_aspirateur' => 'Configurer l\'aspirateur',
	'titre_page_lancer_aspirateur' => 'Lancer l\'aspirateur',
	'titre_lancer_aspirateur_rss' => 'Lancer l\'aspirateur RSS',
	'titre_tester_aspirateur'=> 'Tester l\'aspirateur',

);

?>
