<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file


if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'aspirateur_description' => 'Aspirer les pages d\'un site et les passer, ou pas, en SPIP',
	'aspirateur_nom' => 'Aspirateur',
	'aspirateur_slogan' => 'Aspirez à la sauvegarde des sites'

);
?>